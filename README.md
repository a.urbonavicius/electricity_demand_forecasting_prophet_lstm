# Electricity_demand_forecasting_Prophet_LSTM

Colab Notebook link - https://colab.research.google.com/drive/1lOF5MGOqcmhu9l-M8JW8FkjeZy-iwIxA

Time series analysis and predictions using different approaches.

Analysing continous temporal data and making future predictions is important in many industries and there are different approaches to handle this task. In this notebook I'm going to use 2 different methods and compare their results with an official next day forecast.

The following models will be used:

Prophet - automatic forecasting tool from Facebook. More information here and here.
LSTM - inputing a sequence of data to predict sequence into the future (many-to-many).


Models will be used to predict hourly electricity demand (in MW) in Switzerland.

Dataset is obtained from https://data.open-power-system-data.org/time_series/2017-07-09